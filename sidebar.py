from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QToolBar, QTreeWidget, QPushButton
import qtawesome as qta
from filetree import FileTreeWidget


class SideBar(QWidget):

    def __init__(self):
        super(SideBar, self).__init__()

        self.header = QWidget()
        header_layout = QHBoxLayout()

        self.title = QPushButton("文件管理器")
        self.title.setIcon(qta.icon('mdi.folder-home'))
        self.title.setFlat(True)

        self.dots = QPushButton()
        self.dots.setIcon(qta.icon('mdi.dots-vertical', color='dodgerblue', color_active='orange'))
        self.dots.setFlat(True)

        self.minus = QPushButton()
        self.minus.setIcon(qta.icon('mdi.minus'))
        self.minus.setFlat(True)

        self.splitter = qta.IconWidget('mdi.power-on')

        header_layout.addWidget(self.title)
        header_layout.addStretch(1)
        header_layout.addWidget(self.splitter)
        header_layout.addWidget(self.dots)
        header_layout.addWidget(self.minus)

        self.header.setLayout(header_layout)

        self.nav_tree = FileTreeWidget()

        box = QVBoxLayout()
        box.addWidget(self.header)
        box.addWidget(self.nav_tree, 1)

        self.setLayout(box)
