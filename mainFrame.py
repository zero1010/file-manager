import sys
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, qApp, QApplication, \
    QStatusBar, QMenuBar, QToolBar, QVBoxLayout, QSplitter, QSizePolicy, QTabWidget
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot, Qt
import qtawesome as qta
from frame import Frame
from topbar import TopBar
from page import Page
from sidebar import SideBar
from pageManager import PageManager
from filetree import FileTreeWidget
from dictview import DictView


class MainFrame(Frame):

    def __init__(self):
        super().__init__()
        self.top_bar = TopBar()
        self.client = QSplitter(Qt.Horizontal)
        self.size_bar = FileTreeWidget()
        self.content = PageManager()
        self.client.addWidget(self.size_bar)
        self.client.addWidget(self.content)
        self.size_bar.clicked.connect(self.onFileTreeClicked)
        # self.size_bar.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        # self.client.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        self.client.setStretchFactor(1, 1)
        self.initUI()

    def initUI(self):
        # exit_icon = qta.icon('ei.off', color='blue', color_active='orange')
        # exitAct = QAction(exit_icon, '&Exit', self)
        # exitAct.setShortcut('Ctrl+Q')
        # exitAct.setStatusTip('Exit application')
        # exitAct.triggered.connect(qApp.quit)
        #
        # setting_icon = qta.icon('fa5.sun', color='blue', color_active='orange')
        # settingAct = QAction(setting_icon, '&Setting', self)
        # settingAct.setShortcut('Ctrl+S')
        # settingAct.setStatusTip('应用程序设置')
        # settingAct.triggered.connect(self.openSettings)

        # fileMenu = self.menu_bar.addMenu('&File')
        # fileMenu.addAction(settingAct)
        # fileMenu.addAction(exitAct)
        #
        # self.tool_bar.addAction(settingAct)
        # self.tool_bar.addAction(exitAct)

        self.top_bar.setFixedHeight(self.title_bar_height)
        self.setContentsMargins(0, 0, 0, 0)

        vbox = QVBoxLayout()
        vbox.addWidget(self.top_bar)
        vbox.addWidget(self.client, 1)
        vbox.setContentsMargins(0, 0, 8, 8)
        self.setLayout(vbox)

        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('Main window')
        app_icon = qta.icon('mdi.file-table-box', color='blue', color_active='orange')
        # self.setWindowIcon(QIcon('images/mysql.png'))
        self.setWindowIcon(app_icon)
        self.show()

    @pyqtSlot()
    def onFileTreeClicked(self):
        view = DictView()
        item = self.size_bar.currentItem()
        path = item.file_path
        if path == '/':
            return
        self.content.topView.addTab(view, qta.icon('mdi.folder', color='dodgerblue'), item.text(0))
        view.listDir(path)


def main():
    app = QApplication(sys.argv)
    main_frame = MainFrame()
    main_frame.resize(800, 600)

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
