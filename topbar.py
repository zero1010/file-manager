from PyQt5.QtWidgets import QWidget, QLabel, QHBoxLayout, QPushButton, qApp, QToolButton
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
import qtawesome as qta


class MovableBar(QWidget):
    def __init__(self):
        super(MovableBar, self).__init__()
        self.move_flag = True
        self.move_position = 0

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.move_flag = True
            self.move_position = event.globalPos() - self.parent().pos()  # 获取鼠标相对父窗口的位置
            event.accept()
            self.setCursor(QCursor(Qt.OpenHandCursor))  # 更改鼠标图标

    def mouseMoveEvent(self, event):
        if Qt.LeftButton and self.move_flag:
            self.parent().move(event.globalPos() - self.move_position)  # 更改窗口位置
            event.accept()

    def mouseReleaseEvent(self, event):
        self.move_flag = False
        self.setCursor(QCursor(Qt.ArrowCursor))


class TopBar(MovableBar):
    def __init__(self):
        super(TopBar, self).__init__()

        self.app_ico = qta.IconWidget('mdi.klingon')
        self.title = QLabel('title')

        self.userButton = QPushButton()
        self.userButton.setIcon(qta.icon('mdi.account', color='dodgerblue', color_active='orange'))
        self.userButton.setFlat(True)

        self.themeButton = QPushButton()
        self.themeButton.setIcon(qta.icon('mdi.tshirt-crew-outline', color='dodgerblue', color_active='orange'))
        self.themeButton.setFlat(True)

        self.configButton = QPushButton()
        self.configButton.setIcon(qta.icon('mdi.settings', color='dodgerblue', color_active='orange'))
        self.configButton.setFlat(True)

        self.splitter = qta.IconWidget('mdi.power-on')

        self.minButton = QPushButton()
        self.minButton.setIcon(qta.icon('mdi.window-minimize', color='dodgerblue', color_active='orange'))
        self.minButton.setFlat(True)
        self.minButton.clicked.connect(self.onMinClicked)
        self.maxButton = QPushButton()
        self.maxButton.setIcon(qta.icon('mdi.window-maximize', color='dodgerblue', color_active='orange'))
        self.maxButton.setFlat(True)
        self.maxButton.clicked.connect(self.onMaxClicked)
        self.closeButton = QPushButton()
        self.closeButton.setIcon(qta.icon('mdi.window-close', color='dodgerblue', color_active='orange'))
        self.closeButton.setFlat(True)
        self.closeButton.clicked.connect(qApp.quit)

        box = QHBoxLayout()
        box.addWidget(self.app_ico)
        box.addWidget(self.title, 1)
        # box.addStretch(1)
        box.addWidget(self.userButton)
        box.addWidget(self.themeButton)
        box.addWidget(self.configButton)
        # box.addWidget(self.splitter)
        box.addWidget(self.minButton)
        box.addWidget(self.maxButton)
        box.addWidget(self.closeButton)
        box.setContentsMargins(0, 0, 0, 0)
        box.setSpacing(0)
        # self.setContentsMargins(0, 0, 0, 0)


        self.setLayout(box)
        self.initUI()

    def initUI(self):
        self.setStyleSheet("QWidget {background: seagreen} QPushButton{border:none;background:transparent;} ")


    def onMinClicked(self):
        self.parent().showMinimized()

    def onMaxClicked(self):
        if self.parent().isMaximized():
            self.parent().showNormal()
        else:
            self.parent().showMaximized()
