from PyQt5.QtWidgets import QSplitter, QTabWidget
from PyQt5.QtCore import Qt


class PageManager(QSplitter):
    def __init__(self):
        super(PageManager, self).__init__()
        self.setOrientation(Qt.Vertical)
        self.topView = QTabWidget()
        self.bottomView = QTabWidget()
        self.addWidget(self.topView)
        self.addWidget(self.bottomView)

