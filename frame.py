from PyQt5.QtWidgets import QWidget, QLabel, QPushButton, QVBoxLayout
from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import QFont, QCursor


class Frame(QWidget):
    def __init__(self):
        super(Frame, self).__init__()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self._padding = 5  # 设置边界宽度为5
        self.title_bar_height = 40
        self.move_DragPosition = 0
        self.setMinimumWidth(250)
        self.setMouseTracking(True)
        # self._move_drag = False
        self._corner_drag = False
        self._bottom_drag = False
        self._right_drag = False
        self._right_rect = []
        self._bottom_rect = []
        self._corner_rect = []

    def resizeEvent(self, event):
        # 重新调整边界范围以备实现鼠标拖放缩放， 采用三个列表生成式生成三个列表
        self._right_rect = [QPoint(x, y) for x in range(self.width() - self._padding, self.width() + 1)
                            for y in range(1, self.height() - self._padding)]
        self._bottom_rect = [QPoint(x, y) for x in range(1, self.width() - self._padding)
                             for y in range(self.height() - self._padding, self.height() + 1)]
        self._corner_rect = [QPoint(x, y) for x in range(self.width() - self._padding, self.width() + 1)
                             for y in range(self.height() - self._padding, self.height() + 1)]

    def mousePressEvent(self, event):
        if (event.button() == Qt.LeftButton) and (event.pos() in self._corner_rect):
            # 鼠标左键点击右下角边界区域
            self._corner_drag = True
            event.accept()
        elif (event.button() == Qt.LeftButton) and (event.pos() in self._right_rect):
            #  鼠标左键点击右侧边界区域
            self._right_drag = True
            event.accept()
        elif (event.button() == Qt.LeftButton) and (event.pos() in self._bottom_rect):
            #  鼠标左键点击下侧边界区域
            self._bottom_drag = True
            event.accept()
        # elif (event.button() == Qt.LeftButton) and (event.y() < self.title_bar_height):
            # 鼠标左键点击标题栏区域
        #    self._move_drag = True
        #    self.move_DragPosition = event.globalPos() - self.pos()

    def mouseMoveEvent(self, event):
        # 判断鼠标位置切换鼠标手势
        if event.pos() in self._corner_rect:
            self.setCursor(Qt.SizeFDiagCursor)
        elif event.pos() in self._bottom_rect:
            self.setCursor(Qt.SizeVerCursor)
        elif event.pos() in self._right_rect:
            self.setCursor(Qt.SizeHorCursor)
        else:
            self.setCursor(Qt.ArrowCursor)

        if Qt.LeftButton and self._right_drag:
            self.resize(event.pos().x(), self.height())
            event.accept()
        elif Qt.LeftButton and self._bottom_drag:
            self.resize(self.width(), event.pos().y())
            event.accept()
        # elif Qt.LeftButton and self._move_drag:
        #    self.move(event.globalPos() - self.move_DragPosition)
        #    event.accept()

    def mouseReleaseEvent(self, event):
        # self._move_drag = False
        self._corner_drag = False
        self._bottom_drag = False
        self._right_drag = False


if __name__ == "__main__":
    from PyQt5.QtWidgets import QApplication
    import sys

    app = QApplication(sys.argv)
    # app.setStyleSheet(open("./UnFrameStyle.qss").read())
    window = Frame()
    # window.setCloseButton(True)
    # window.setMinMaxButtons(True)
    window.show()
    sys.exit(app.exec_())
